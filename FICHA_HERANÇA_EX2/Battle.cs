﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FICHA_HERANÇA_EX2
{
    public partial class Battle : Form
    {

        List<Criatura> Decks = new List<Criatura>();
        List<string> Imagens = new List<string>();
        List<Criatura> MC = new List<Criatura>();
        List<Criatura> EN = new List<Criatura>();
        List<string> im = new List<string>();
        public int PC_Pick, EN_Pick;

        public Battle(List<Criatura> Criatura, List<string> Pics)
        {

            InitializeComponent();
            for (int i = 0; i < 5; i++)
            {
                im.Add("");
            }
            Decks = Criatura;
            Imagens = Pics;
            EscolherCartas();
            Button_normal.BackColor = Color.DarkGray;
            Button_normal.ForeColor = Color.White;
            Button_easy.BackColor = Color.Empty;
            Button_easy.ForeColor = Color.Black;
            Button_Hard.BackColor = Color.Empty;
            Button_Hard.ForeColor = Color.Black;

        }

        private void PictureBox_Player_Card1_Click(object sender, EventArgs e)
        {
            PictureBox_Player.Image = PictureBox_Player_Card1.Image;
            PC_Pick = 0;
            EnemyPickCard();

            Button_easy.Enabled = false;
            Button_normal.Enabled = false;
            Button_Hard.Enabled = false;

            //var x = PictureBox_Player.Width;
            //PictureBox_Player.Width = PictureBox_Player.Height;
            //PictureBox_Player.Height = x;
            //PictureBox_Player.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);

        }

        private void PictureBox_Player_Card2_Click(object sender, EventArgs e)
        {
            PictureBox_Player.Image = PictureBox_Player_Card2.Image;
            PC_Pick = 1;
            EnemyPickCard();

            Button_easy.Enabled = false;
            Button_normal.Enabled = false;
            Button_Hard.Enabled = false;
        }

        private void PictureBox_Player_Card3_Click(object sender, EventArgs e)
        {
            PictureBox_Player.Image = PictureBox_Player_Card3.Image;
            PC_Pick = 2;
            EnemyPickCard();

            Button_easy.Enabled = false;
            Button_normal.Enabled = false;
            Button_Hard.Enabled = false;
        }

        private void PictureBox_Player_Card4_Click(object sender, EventArgs e)
        {
            PictureBox_Player.Image = PictureBox_Player_Card4.Image;
            PC_Pick = 3;
            EnemyPickCard();

            Button_easy.Enabled = false;
            Button_normal.Enabled = false;
            Button_Hard.Enabled = false;
        }

        private void PictureBox_Player_Card5_Click(object sender, EventArgs e)
        {
            PictureBox_Player.Image = PictureBox_Player_Card5.Image;
            PC_Pick = 4;
            EnemyPickCard();

            Button_easy.Enabled = false;
            Button_normal.Enabled = false;
            Button_Hard.Enabled = false;
        }

        private void Button_Attack_Click(object sender, EventArgs e)
        {

            
            #region

            if (MC[PC_Pick].Attack >= EN[EN_Pick].Resistance)
            {

                MessageBox.Show("A carta do inimigo foi eliminada!");
                switch (EN_Pick)
                {
                    case 0:
                        PictureBox_Enemy_Card1.Visible = false;
                        break;
                    case 1:
                        PictureBox_Enemy_Card2.Visible = false;
                        break;
                    case 2:
                        PictureBox_Enemy_Card3.Visible = false;
                        break;
                    case 3:
                        PictureBox_Enemy_Card4.Visible = false;
                        break;
                    case 4:
                        PictureBox_Enemy_Card5.Visible = false;
                        break;
                }
                PictureBox_Oponent.Image = null;
                Button_Attack.Enabled = false;

                Label_HPEnemy.Text = (Convert.ToInt16(Label_HPEnemy.Text) - MC[PC_Pick].Attack).ToString();
                if (Convert.ToInt16((Label_HPEnemy.Text)) <= 0)
                {

                    MessageBox.Show("Você ganhou! Seu adversário perdeu e sua time ganhou!");
                    this.Close();

                    return;

                }

            }
            else
            {
                MessageBox.Show("Você é muito mau mesmo não é? Tá na hora do seu inimigo jogar!");
                EnemyAttack();
            }

            if (!PictureBox_Enemy_Card1.Visible && !PictureBox_Enemy_Card2.Visible && !PictureBox_Enemy_Card3.Visible && !PictureBox_Enemy_Card4.Visible && !PictureBox_Enemy_Card5.Visible)
            {
                MessageBox.Show("Você ganhou! Seu adversário era mau e você eliminou todas as suas cartas! Bis!");
                this.Close();

            }
            else if (!PictureBox_Player_Card1.Visible && !PictureBox_Player_Card2.Visible && !PictureBox_Player_Card3.Visible && !PictureBox_Player_Card4.Visible && !PictureBox_Player_Card5.Visible)
            {
                MessageBox.Show("Você nunca devia ser deixado de voltar a jogareste game de novo! As suas cartas foram vaporizadas pelo inimigo!");
                this.Close();

            }

            #endregion


        }

        private void button1_Click(object sender, EventArgs e)
        {

            Button_easy.Enabled = true;
            Button_normal.Enabled = true;
            Button_Hard.Enabled = true;

            PictureBox_Player_Card1.Visible = true;
            PictureBox_Player_Card2.Visible = true;
            PictureBox_Player_Card3.Visible = true;
            PictureBox_Player_Card4.Visible = true;
            PictureBox_Player_Card5.Visible = true;
            PictureBox_Enemy_Card1.Visible = true;
            PictureBox_Enemy_Card2.Visible = true;
            PictureBox_Enemy_Card3.Visible = true;
            PictureBox_Enemy_Card4.Visible = true;
            PictureBox_Enemy_Card5.Visible = true;

            Label_HPEnemy.Text = "35";
            HP_Player.Text = "35";
            MC.Clear();
            EN.Clear();

            EscolherCartas();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Button_normal.BackColor = Color.Empty;
            Button_normal.ForeColor = Color.Black;
            Button_Hard.BackColor = Color.Empty;
            Button_Hard.ForeColor = Color.Black;
            HP_Player.Text = "45";
            Label_HPEnemy.Text = "25";
            Button_easy.BackColor = Color.DarkGray;
            Button_easy.ForeColor = Color.White;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Button_easy.BackColor = Color.Empty;
            Button_easy.ForeColor = Color.Black;
            Button_Hard.BackColor = Color.Empty;
            Button_Hard.ForeColor = Color.Black;
            HP_Player.Text = "35";
            Label_HPEnemy.Text = "35";
            Button_normal.BackColor = Color.DarkGray;
            Button_normal.ForeColor = Color.White;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Button_normal.BackColor = Color.Empty;
            Button_normal.ForeColor = Color.Black;
            Button_easy.BackColor = Color.Empty;
            Button_easy.ForeColor = Color.Black;
            HP_Player.Text = "25";
            Label_HPEnemy.Text = "45";
            Button_Hard.BackColor = Color.DarkGray;
            Button_Hard.ForeColor = Color.White;
            

        }

        private void Battle_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form_Magic form = new Form_Magic();
            form.Show();
        }

        #region métodos:

        private void EnemyPickCard()
        {

            Random al = new Random();
            voltar:
            var x = al.Next(0, 9);

            switch (x)
            {
                case 5:
                case 0:
                    if (PictureBox_Enemy_Card1.Visible)
                    {
                        PictureBox_Oponent.ImageLocation = im[0];
                        EN_Pick = 0;
                    }
                    else
                    {
                        goto voltar;
                    }
                    break;
                case 6:
                case 1:
                    if (PictureBox_Enemy_Card2.Visible)
                    {
                        PictureBox_Oponent.ImageLocation = im[1];
                        EN_Pick = 1;
                    }
                    else
                    {
                        goto voltar;
                    }
                    break;
                case 7:
                case 2:
                    if (PictureBox_Enemy_Card3.Visible)
                    {
                        PictureBox_Oponent.ImageLocation = im[2];
                        EN_Pick = 2;
                    }
                    else
                    {
                        goto voltar;
                    }
                    break;
                case 8:
                case 3:
                    if (PictureBox_Enemy_Card4.Visible)
                    {
                        PictureBox_Oponent.ImageLocation = im[3];
                        EN_Pick = 3;
                    }
                    else
                    {
                        goto voltar;
                    }
                    break;
                case 9:
                case 4:
                    if (PictureBox_Enemy_Card5.Visible)
                    {
                        PictureBox_Oponent.ImageLocation = im[4];
                        EN_Pick = 4;
                    }
                    else
                    {
                        goto voltar;
                    }
                    break;
            }

            Button_Attack.Enabled = true;

        }

        private void EscolherCartas()
        {

            Random aleatorio = new Random();

            for (int i = 0; i < 5; i++)
            {
                var x = aleatorio.Next(0, 14);
                MC.Add(Decks[x]);

                switch (i)
                {
                    case 0:
                        PictureBox_Player_Card1.ImageLocation = Imagens[x];
                        break;
                    case 1:
                        PictureBox_Player_Card2.ImageLocation = Imagens[x];
                        break;
                    case 2:
                        PictureBox_Player_Card3.ImageLocation = Imagens[x];
                        break;
                    case 3:
                        PictureBox_Player_Card4.ImageLocation = Imagens[x];
                        break;
                    case 4:
                        PictureBox_Player_Card5.ImageLocation = Imagens[x];
                        break;
                }

                var y = aleatorio.Next(0, 14);

                EN.Add(Decks[y]);

                switch (i)
                {
                    case 0:
                        PictureBox_Enemy_Card1.ImageLocation = Imagens[y];
                        im[0] = PictureBox_Enemy_Card1.ImageLocation;
                        break;
                    case 1:
                        PictureBox_Enemy_Card2.ImageLocation = Imagens[y];
                        im[1] = PictureBox_Enemy_Card2.ImageLocation;
                        break;
                    case 2:
                        PictureBox_Enemy_Card3.ImageLocation = Imagens[y];
                        im[2] = PictureBox_Enemy_Card3.ImageLocation;
                        break;
                    case 3:
                        PictureBox_Enemy_Card4.ImageLocation = Imagens[y];
                        im[3] = PictureBox_Enemy_Card4.ImageLocation;
                        break;
                    case 4:
                        PictureBox_Enemy_Card5.ImageLocation = Imagens[y];
                        im[4] = PictureBox_Enemy_Card5.ImageLocation;
                        break;
                }

                PictureBox_Enemy_Card1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\BackCard.jpg";
                PictureBox_Enemy_Card2.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\BackCard.jpg";
                PictureBox_Enemy_Card3.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\BackCard.jpg";
                PictureBox_Enemy_Card4.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\BackCard.jpg";
                PictureBox_Enemy_Card5.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\BackCard.jpg";

            }

        }

        private void EnemyAttack()
        {

            if (EN[EN_Pick].Attack >= MC[PC_Pick].Resistance)
            {

                MessageBox.Show("A sua carta da poia foi eliminada!");
                switch (PC_Pick)
                {
                    case 0:
                        PictureBox_Player_Card1.Visible = false;
                        break;
                    case 1:
                        PictureBox_Player_Card2.Visible = false;
                        break;
                    case 2:
                        PictureBox_Player_Card3.Visible = false;
                        break;
                    case 3:
                        PictureBox_Player_Card4.Visible = false;
                        break;
                    case 4:
                        PictureBox_Player_Card5.Visible = false;
                        break;
                }
                PictureBox_Player.Image = null;
                Button_Attack.Enabled = false;
                HP_Player.Text = (Convert.ToInt16(HP_Player.Text) - EN[EN_Pick].Attack).ToString();

                if (Convert.ToInt16((HP_Player.Text)) <= 0)
                {

                    MessageBox.Show("Você é muito mau, vá chorar no canto e pense no que fez de errado!");
                    this.Close();
                    return;

                }

            }
            else
            {
                MessageBox.Show("Seu inimigo é tão mau quanto você, sua vez!");
            }

        }

        #endregion

    }

}
