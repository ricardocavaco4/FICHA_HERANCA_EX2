﻿namespace FICHA_HERANÇA_EX2
{
    partial class Battle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureBox_Player = new System.Windows.Forms.PictureBox();
            this.PictureBox_Oponent = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.HP_Player = new System.Windows.Forms.Label();
            this.Label_HPEnemy = new System.Windows.Forms.Label();
            this.PictureBox_Player_Card1 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Player_Card2 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Player_Card3 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Player_Card4 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Player_Card5 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Enemy_Card5 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Enemy_Card4 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Enemy_Card3 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Enemy_Card2 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Enemy_Card1 = new System.Windows.Forms.PictureBox();
            this.Button_Attack = new System.Windows.Forms.Button();
            this.Button_Refresh = new System.Windows.Forms.Button();
            this.Button_easy = new System.Windows.Forms.Button();
            this.Button_normal = new System.Windows.Forms.Button();
            this.Button_Hard = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Oponent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card1)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBox_Player
            // 
            this.PictureBox_Player.Location = new System.Drawing.Point(12, 134);
            this.PictureBox_Player.Name = "PictureBox_Player";
            this.PictureBox_Player.Size = new System.Drawing.Size(273, 320);
            this.PictureBox_Player.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player.TabIndex = 7;
            this.PictureBox_Player.TabStop = false;
            // 
            // PictureBox_Oponent
            // 
            this.PictureBox_Oponent.Location = new System.Drawing.Point(414, 134);
            this.PictureBox_Oponent.Name = "PictureBox_Oponent";
            this.PictureBox_Oponent.Size = new System.Drawing.Size(273, 320);
            this.PictureBox_Oponent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Oponent.TabIndex = 8;
            this.PictureBox_Oponent.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Jogador 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(593, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Jogador 2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "Vida:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(648, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = ":Vida";
            // 
            // HP_Player
            // 
            this.HP_Player.AutoSize = true;
            this.HP_Player.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HP_Player.Location = new System.Drawing.Point(53, 38);
            this.HP_Player.Name = "HP_Player";
            this.HP_Player.Size = new System.Drawing.Size(24, 16);
            this.HP_Player.TabIndex = 13;
            this.HP_Player.Text = "35";
            // 
            // Label_HPEnemy
            // 
            this.Label_HPEnemy.AutoSize = true;
            this.Label_HPEnemy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_HPEnemy.Location = new System.Drawing.Point(618, 38);
            this.Label_HPEnemy.Name = "Label_HPEnemy";
            this.Label_HPEnemy.Size = new System.Drawing.Size(24, 16);
            this.Label_HPEnemy.TabIndex = 14;
            this.Label_HPEnemy.Text = "35";
            // 
            // PictureBox_Player_Card1
            // 
            this.PictureBox_Player_Card1.Location = new System.Drawing.Point(11, 68);
            this.PictureBox_Player_Card1.Name = "PictureBox_Player_Card1";
            this.PictureBox_Player_Card1.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Player_Card1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player_Card1.TabIndex = 15;
            this.PictureBox_Player_Card1.TabStop = false;
            this.PictureBox_Player_Card1.Click += new System.EventHandler(this.PictureBox_Player_Card1_Click);
            // 
            // PictureBox_Player_Card2
            // 
            this.PictureBox_Player_Card2.Location = new System.Drawing.Point(67, 68);
            this.PictureBox_Player_Card2.Name = "PictureBox_Player_Card2";
            this.PictureBox_Player_Card2.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Player_Card2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player_Card2.TabIndex = 16;
            this.PictureBox_Player_Card2.TabStop = false;
            this.PictureBox_Player_Card2.Click += new System.EventHandler(this.PictureBox_Player_Card2_Click);
            // 
            // PictureBox_Player_Card3
            // 
            this.PictureBox_Player_Card3.Location = new System.Drawing.Point(123, 68);
            this.PictureBox_Player_Card3.Name = "PictureBox_Player_Card3";
            this.PictureBox_Player_Card3.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Player_Card3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player_Card3.TabIndex = 17;
            this.PictureBox_Player_Card3.TabStop = false;
            this.PictureBox_Player_Card3.Click += new System.EventHandler(this.PictureBox_Player_Card3_Click);
            // 
            // PictureBox_Player_Card4
            // 
            this.PictureBox_Player_Card4.Location = new System.Drawing.Point(179, 68);
            this.PictureBox_Player_Card4.Name = "PictureBox_Player_Card4";
            this.PictureBox_Player_Card4.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Player_Card4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player_Card4.TabIndex = 18;
            this.PictureBox_Player_Card4.TabStop = false;
            this.PictureBox_Player_Card4.Click += new System.EventHandler(this.PictureBox_Player_Card4_Click);
            // 
            // PictureBox_Player_Card5
            // 
            this.PictureBox_Player_Card5.Location = new System.Drawing.Point(235, 68);
            this.PictureBox_Player_Card5.Name = "PictureBox_Player_Card5";
            this.PictureBox_Player_Card5.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Player_Card5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Player_Card5.TabIndex = 19;
            this.PictureBox_Player_Card5.TabStop = false;
            this.PictureBox_Player_Card5.Click += new System.EventHandler(this.PictureBox_Player_Card5_Click);
            // 
            // PictureBox_Enemy_Card5
            // 
            this.PictureBox_Enemy_Card5.Location = new System.Drawing.Point(637, 68);
            this.PictureBox_Enemy_Card5.Name = "PictureBox_Enemy_Card5";
            this.PictureBox_Enemy_Card5.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Enemy_Card5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Enemy_Card5.TabIndex = 24;
            this.PictureBox_Enemy_Card5.TabStop = false;
            // 
            // PictureBox_Enemy_Card4
            // 
            this.PictureBox_Enemy_Card4.Location = new System.Drawing.Point(581, 68);
            this.PictureBox_Enemy_Card4.Name = "PictureBox_Enemy_Card4";
            this.PictureBox_Enemy_Card4.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Enemy_Card4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Enemy_Card4.TabIndex = 23;
            this.PictureBox_Enemy_Card4.TabStop = false;
            // 
            // PictureBox_Enemy_Card3
            // 
            this.PictureBox_Enemy_Card3.Location = new System.Drawing.Point(525, 68);
            this.PictureBox_Enemy_Card3.Name = "PictureBox_Enemy_Card3";
            this.PictureBox_Enemy_Card3.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Enemy_Card3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Enemy_Card3.TabIndex = 22;
            this.PictureBox_Enemy_Card3.TabStop = false;
            // 
            // PictureBox_Enemy_Card2
            // 
            this.PictureBox_Enemy_Card2.Location = new System.Drawing.Point(469, 68);
            this.PictureBox_Enemy_Card2.Name = "PictureBox_Enemy_Card2";
            this.PictureBox_Enemy_Card2.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Enemy_Card2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Enemy_Card2.TabIndex = 21;
            this.PictureBox_Enemy_Card2.TabStop = false;
            // 
            // PictureBox_Enemy_Card1
            // 
            this.PictureBox_Enemy_Card1.Location = new System.Drawing.Point(413, 68);
            this.PictureBox_Enemy_Card1.Name = "PictureBox_Enemy_Card1";
            this.PictureBox_Enemy_Card1.Size = new System.Drawing.Size(50, 60);
            this.PictureBox_Enemy_Card1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Enemy_Card1.TabIndex = 20;
            this.PictureBox_Enemy_Card1.TabStop = false;
            // 
            // Button_Attack
            // 
            this.Button_Attack.Enabled = false;
            this.Button_Attack.Location = new System.Drawing.Point(291, 431);
            this.Button_Attack.Name = "Button_Attack";
            this.Button_Attack.Size = new System.Drawing.Size(117, 23);
            this.Button_Attack.TabIndex = 25;
            this.Button_Attack.Text = "Atacar";
            this.Button_Attack.UseVisualStyleBackColor = true;
            this.Button_Attack.Click += new System.EventHandler(this.Button_Attack_Click);
            // 
            // Button_Refresh
            // 
            this.Button_Refresh.Location = new System.Drawing.Point(291, 6);
            this.Button_Refresh.Name = "Button_Refresh";
            this.Button_Refresh.Size = new System.Drawing.Size(117, 23);
            this.Button_Refresh.TabIndex = 26;
            this.Button_Refresh.Text = "Refresh";
            this.Button_Refresh.UseVisualStyleBackColor = true;
            this.Button_Refresh.Click += new System.EventHandler(this.button1_Click);
            // 
            // Button_easy
            // 
            this.Button_easy.Location = new System.Drawing.Point(291, 195);
            this.Button_easy.Name = "Button_easy";
            this.Button_easy.Size = new System.Drawing.Size(117, 23);
            this.Button_easy.TabIndex = 27;
            this.Button_easy.Text = "Fácil";
            this.Button_easy.UseVisualStyleBackColor = true;
            this.Button_easy.Click += new System.EventHandler(this.button2_Click);
            // 
            // Button_normal
            // 
            this.Button_normal.Location = new System.Drawing.Point(291, 222);
            this.Button_normal.Name = "Button_normal";
            this.Button_normal.Size = new System.Drawing.Size(117, 23);
            this.Button_normal.TabIndex = 28;
            this.Button_normal.Text = "Normal";
            this.Button_normal.UseVisualStyleBackColor = true;
            this.Button_normal.Click += new System.EventHandler(this.button3_Click);
            // 
            // Button_Hard
            // 
            this.Button_Hard.Location = new System.Drawing.Point(291, 251);
            this.Button_Hard.Name = "Button_Hard";
            this.Button_Hard.Size = new System.Drawing.Size(117, 23);
            this.Button_Hard.TabIndex = 29;
            this.Button_Hard.Text = "Dificíl";
            this.Button_Hard.UseVisualStyleBackColor = true;
            this.Button_Hard.Click += new System.EventHandler(this.button4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(289, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 20);
            this.label5.TabIndex = 30;
            this.label5.Text = "  Dificuldade  ";
            // 
            // Battle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 466);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Button_Hard);
            this.Controls.Add(this.Button_normal);
            this.Controls.Add(this.Button_easy);
            this.Controls.Add(this.Button_Refresh);
            this.Controls.Add(this.Button_Attack);
            this.Controls.Add(this.PictureBox_Enemy_Card5);
            this.Controls.Add(this.PictureBox_Enemy_Card4);
            this.Controls.Add(this.PictureBox_Enemy_Card3);
            this.Controls.Add(this.PictureBox_Enemy_Card2);
            this.Controls.Add(this.PictureBox_Enemy_Card1);
            this.Controls.Add(this.PictureBox_Player_Card5);
            this.Controls.Add(this.PictureBox_Player_Card4);
            this.Controls.Add(this.PictureBox_Player_Card3);
            this.Controls.Add(this.PictureBox_Player_Card2);
            this.Controls.Add(this.PictureBox_Player_Card1);
            this.Controls.Add(this.Label_HPEnemy);
            this.Controls.Add(this.HP_Player);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PictureBox_Oponent);
            this.Controls.Add(this.PictureBox_Player);
            this.Name = "Battle";
            this.Text = "Battle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Battle_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Oponent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Player_Card5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Enemy_Card1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox_Player;
        private System.Windows.Forms.PictureBox PictureBox_Oponent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label HP_Player;
        private System.Windows.Forms.Label Label_HPEnemy;
        private System.Windows.Forms.PictureBox PictureBox_Player_Card1;
        private System.Windows.Forms.PictureBox PictureBox_Player_Card2;
        private System.Windows.Forms.PictureBox PictureBox_Player_Card3;
        private System.Windows.Forms.PictureBox PictureBox_Player_Card4;
        private System.Windows.Forms.PictureBox PictureBox_Player_Card5;
        private System.Windows.Forms.PictureBox PictureBox_Enemy_Card5;
        private System.Windows.Forms.PictureBox PictureBox_Enemy_Card4;
        private System.Windows.Forms.PictureBox PictureBox_Enemy_Card3;
        private System.Windows.Forms.PictureBox PictureBox_Enemy_Card2;
        private System.Windows.Forms.PictureBox PictureBox_Enemy_Card1;
        private System.Windows.Forms.Button Button_Attack;
        private System.Windows.Forms.Button Button_Refresh;
        private System.Windows.Forms.Button Button_easy;
        private System.Windows.Forms.Button Button_normal;
        private System.Windows.Forms.Button Button_Hard;
        private System.Windows.Forms.Label label5;
    }
}