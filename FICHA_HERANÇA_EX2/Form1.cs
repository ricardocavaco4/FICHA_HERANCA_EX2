﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FICHA_HERANÇA_EX2
{
    public partial class Form_Magic : Form
    {

        List<string> pictures = new List<string>();

        List<Criatura> Lista_Criaturas = new List<Criatura>();
        List<Artefacto> Lista_Artefactos = new List<Artefacto>();
        List<Encantamento> Lista_Encantamentos = new List<Encantamento>();
        List<Feitiço> Lista_Feitiços = new List<Feitiço>();
        List<MagicaInstantanea> Lista_MagInst = new List<MagicaInstantanea>();
        List<Terreno> Lista_Terrenos = new List<Terreno>();

        public Form_Magic()
        {

            InitializeComponent();
            CarregarListas();

        }

        private void CarregarListas()
        {

            Criatura c = new Criatura();
            Terreno t = new Terreno();
            Artefacto a = new Artefacto();
            MagicaInstantanea m = new MagicaInstantanea();
            Feitiço f = new Feitiço();
            Encantamento e = new Encantamento();

            Lista_Criaturas = c.Criar_Criaturas();
            Lista_Terrenos = t.Criar_Terrenos();
            Lista_Artefactos = a.Criar_Artefactos();
            Lista_MagInst = m.Criar_MagicaInstantanea();
            Lista_Feitiços = f.Criar_Feitiços();
            Lista_Encantamentos = e.Criar_Encantamentos();

            for (int i = 0; i < 15; i++)
            {
                pictures.Add("");
            }

            #region pictures       
            
            pictures[0] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Urso_Selvagem.png"; 
            pictures[1] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Mandragora.png";
            pictures[2] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Louva_Deus.png";
            pictures[3] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Corpo_Ardente.png";
            pictures[4] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Fornalha_Viva.png";
            pictures[5] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Seth.png";
            pictures[6] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Bimbi.png";
            pictures[7] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Lagartixa.png";
            pictures[8] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Remiel.png";
            pictures[9] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Lucifer.png";
            pictures[10] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Zombie.png";
            pictures[11] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Alma_Perdida.png";
            pictures[12] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Tubarão_Malhado.png";
            pictures[13] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Abutre_Veloz.png";
            pictures[14] = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Febre_Louca.png";            

            #endregion


        }

        private void button1_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_Criaturas;
            comboBox1.DisplayMember = "Nome";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_Terrenos;
            comboBox1.DisplayMember = "Nome";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_Encantamentos;
            comboBox1.DisplayMember = "Nome";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_Feitiços;
            comboBox1.DisplayMember = "Nome";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_MagInst;
            comboBox1.DisplayMember = "Nome";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Lista_Artefactos;
            comboBox1.DisplayMember = "Nome";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboBox1.DataSource != null)
            {

                var x = comboBox1.SelectedValue as Carta;
                descriçao.Text = x.ToString();

                if (x is Criatura)
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Febre Louca":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Febre_Louca.png";
                                break;
                            }
                        case "Urso Selvagem":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Urso_Selvagem.png";
                                break;
                            }
                        case "Abutre Veloz":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Abutre_Veloz.png";
                                break;
                            }
                        case "Tubarão Malhado":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Tubarão_Malhado.png";

                                break;
                            }
                        case "Corpo Ardente":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Corpo_Ardente.png";

                                break;
                            }
                        case "Fornalha Viva":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Fornalha_Viva.png";

                                break;
                            }
                        case "Alma Perdida":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Alma_Perdida.png";

                                break;
                            }
                        case "Remiel":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Remiel.png";

                                break;
                            }
                        case "Mandragora":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Mandragora.png";

                                break;
                            }
                        case "Seth, o Dragão Negro":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Seth.png";

                                break;
                            }
                        case "Louva-Deus":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Louva_Deus.png";

                                break;
                            }
                        case "Zombie PopStar":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Zombie.png";

                                break;
                            }
                        case "Lucifer":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Lucifer.png";

                                break;
                            }
                        case "Bimbi":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Bimbi.png";

                                break;
                            }
                        case "Lagartixa Albina":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Lagartixa.png";

                                break;
                            }
                    }
                    #endregion

                }
                else if (x is Terreno)
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Rio":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Rio.png";
                                break;
                            }
                        case "Vulcão":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Vulcão.png";
                                break;
                            }
                        case "Floresta":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Floresta.png";
                                break;
                            }
                        case "Pântano":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Pântano.png";
                                break;
                            }
                        case "Planície":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Planície.png";
                                break;
                            }

                    }
                    #endregion

                }
                else if (x is Artefacto)
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Ramo de Yggddrasil":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Tree.png";
                                break;
                            }
                        case "Dente de Tigre":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Tiger.png";
                                break;
                            }
                        case "Cálice da Loja dos Trezentos":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Calice.png";
                                break;
                            }
                        case "Diário de um Mentiroso":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Diary.png";
                                break;
                            }
                        case "Galo de Barcelos":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Galo.png";
                                break;
                            }

                    }
                    #endregion

                }
                else if (x is Feitiço)
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Chuva de Pedragulhos":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Chuva.png";
                                break;
                            }
                        case "Cinzas Fedorentas":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Ash.png";
                                break;
                            }
                        case "Saliva Restauradora":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Saliva.png";
                                break;
                            }
                        case "Nevoeiro Sinistro":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Nevoeiro.png";
                                break;
                            }
                        case "Espuma de banho":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Espuma.png";
                                break;
                            }

                    }
                    #endregion

                }
                else if (x is MagicaInstantanea)
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Bola de Fogo Rápida":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Fireball.png";
                                break;
                            }
                        case "Toxinas":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Toxin.png";
                                break;
                            }
                        case "Interromper Luz":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Blind.png";
                                break;
                            }
                        case "Buraco Negro":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Hole.png";
                                break;
                            }
                        case "Lágrimas de Crocodilo":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Tears.png";
                                break;
                            }

                    }
                    #endregion

                }
                else//Encantamento
                {

                    #region
                    switch (x.Nome)
                    {
                        case "Sussuro Vitalício":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Sussurro.png";
                                break;
                            }
                        case "Grito de Dor":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Grito_de_Dor.png";
                                break;
                            }
                        case "Círculo de Chamas":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Circulo_de_Chamas.png";
                                break;
                            }
                        case "Proteção do Urso":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Proteção_de_Urso.png";
                                break;
                            }
                        case "Roubar":
                            {
                                pictureBox1.ImageLocation = @"E:\CINEL\FICHA_HERANÇA_EX2\FICHA_HERANÇA_EX2\Imagens\Roubar.png";
                                break;
                            }

                    }
                    #endregion

                }

            }
        }

        private void Button_Luta_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Atenção, o Lucifer tem 5 de ataque e 7 de resistência!");
            Battle Battle = new Battle(Lista_Criaturas, pictures);
            Battle.Show();
            this.Hide();

        }

        private void Form_Magic_Load(object sender, EventArgs e)
        {

        }

        private void Form_Magic_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
