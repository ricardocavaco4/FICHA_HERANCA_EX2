﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    class Artefacto : Carta
    {
        private string hability;
        private string Descricao;

        public Artefacto(ElementoCarta el, int custo, string nome, string desc, string habilidade, string des):base( el,custo,nome,desc)
        {
            hability = habilidade;
            Descricao = des;

        }

        public Artefacto()
        {
        }

        public override string ToString()
        {

            string elemento = "Sem Cor";
            return $"» Artefacto:\n\n   Nome: {Nome}\n   Cor: {elemento}\n   Custo: {Mana}\n\n   {Descricao}\n   \n\n   Descrição: {DescricaoCarta}\n\n \n";

        }
        public List<Artefacto> Criar_Artefactos()
        {

            List<Artefacto> Artefactos = new List<Artefacto>();

            var x1 = new Artefacto(ElementoCarta.Sem_Cor, 2, "Ramo de Yggddrasil", "O ramo da árvore da vida Yggddrasil.", "Benção", "A criatura recebe +2/+2");
            var x2 = new Artefacto(ElementoCarta.Sem_Cor, 2, "Dente de Tigre", "O dente de um gato maior que os outros...", "Rugido", "A criatura não pode ser atacada por criaturas do tipo Animal.");
            var x3 = new Artefacto(ElementoCarta.Sem_Cor, 2, "Cálice da Loja dos Trezentos", "Um cálice bonito que parece feito de ouro mas é feito de plástico...", "Beber àgua", "A criatura recebe +1/+1 por cada encantamento conjurado nela.");
            var x4 = new Artefacto(ElementoCarta.Sem_Cor, 2, "Diário de um Mentiroso", "O diário de um mentiroso executado por roubar batatas.", "Ler Entradas", "A criatura recebe um bonús de +1/+0 por cada criatura destruída por esta.");
            var x5 = new Artefacto(ElementoCarta.Sem_Cor, 2, "Galo de Barcelos", "Uma peça icónica Portuguesa.", "Nobre Povo", "A criatura canta o hino nacional e não pode ser atacada a cada 2 turnos.");

            Artefactos.Add(x1);
            Artefactos.Add(x2);
            Artefactos.Add(x3);
            Artefactos.Add(x4);
            Artefactos.Add(x5);


            return Artefactos;
        }

    }
}
