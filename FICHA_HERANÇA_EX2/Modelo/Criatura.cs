﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    public class Criatura : Carta
    {

        public Tipo_Criatura Tipo { get; set; }
        public int Resistance
        {
            get
            {
                return Resistência;
            }
            set
            {
                Resistance = Resistência;
            }
        }

        public int Attack
        {
            get
            {
                return Ataque;
            }
            set
            {
                Attack = Ataque;
            }
        }

        private int Ataque;
        private int Resistência;
        private string Habilidade1;
        private string Descricao1;
        private string Habilidade2;
        private string Descricao2;

        public Criatura(ElementoCarta el, int custo, string nome, string desc, int atk, int def, string habilidade1, string habilidade2, string des1, string des2, Tipo_Criatura tipo) : base(el, custo, nome, desc)
        {

            Ataque = atk;
            Resistência = def;
            Habilidade1 = habilidade1;
            Habilidade2 = habilidade2;
            Descricao1 = des1;
            Descricao2 = des2;
            Tipo = tipo;

        }

        public Criatura()
        {
        }

        public override string ToString()
        {

            return $"» Criatura:\n\n   Nome: {Tipo} - {Nome}\n   Cor: {Elemento}\n   Custo: {Mana}\n\n   {Habilidade1}: {Descricao1}\n   {Habilidade2}: {Descricao2}\n\n    Descrição: {DescricaoCarta}\n\n    ATK/RES: {Ataque}/{Resistência}\n";

        }

        public List<Criatura> Criar_Criaturas()
        {

            List<Criatura> Criaturas = new List<Criatura>();

            var x1 = new Criatura(ElementoCarta.Floresta, 2, "Urso Selvagem","Uma criatura assustadora e forte, capaz de proteger todos.", 2, 3,
                "Arrancar Asas","", "Todas as criaturas do tipo Voador recebem um dano bonús de -1/+0.","", Tipo_Criatura.Animal);

            var x2 = new Criatura(ElementoCarta.Floresta, 1, "Mandragora", "Uma erva pequena que rompe os tímpanos de todos os predadores.", 1, 1, 
                "Grito", "Veneno", "A criatura alvo recebe +0/-1.", "A criatura alvo tem 40% chance de perder +0/-1 por turno.", Tipo_Criatura.Planta);

            var x3 = new Criatura(ElementoCarta.Floresta, 2, "Louva-Deus", "Um inseto que não mete medo a ninguém mas é bem mortífero.", 3, 0, 
                "Cortar", "", "Todas as criaturas do oponente recebem -1/+0 e esta criatura também.", "", Tipo_Criatura.Inseto);

            var x4 = new Criatura(ElementoCarta.Fogo, 1, "Corpo Ardente","Um cadáver fraco que se incendiou sozinho e começou a ganhar consciência e um sentimento de vingança.",1,0, 
                "Chamas Fracas","","Danos causados a esta criatura por parte de criaturas do tipo Fogo, é retirado +0/-1","",Tipo_Criatura.Humanóide);

            var x5 = new Criatura(ElementoCarta.Fogo, 2, "Fornalha Viva", "Uma fornalha possuída por um espiríto de um bolo que ficou queimado e agora quer vingança.", 2, 1, 
                "Queimar", "Cozinhar", "Queima a criatura defensiva se esta for do tipo Floresta para receber danos bonús +0/-2.", "Esta criatura recebe +2/+2(Uma vez por jogo).", Tipo_Criatura.Objeto);

            var x6 = new Criatura(ElementoCarta.Fogo, 4, "Seth, o Dragão Negro", "Um dragão encoberto em mistério e poder.", 5, 4, 
                "Voador", "Lança-Chamas", "Não pode ser atingido por ataques térreos.", "Todas as criaturas que não sejam do tipo Fogo recebem +0/-3.", Tipo_Criatura.Dragão);

            var x7 = new Criatura(ElementoCarta.Planície, 1, "Bimbi", "O robô de cozinha do ano escolhido pelos portugueses!", 1, 1, 
                "Cozinhar", "", "Esta criatura recebe +2/+2(Uma vez por jogo).", "", Tipo_Criatura.Mecânico);

            var x8 = new Criatura(ElementoCarta.Planície, 1, "Lagartixa Albina", "Esta variação rara é excepcionalmente melhor que a lagartixa normal.", 1, 2, 
                "Apanhar Sol", "", "Esta criatura ganha +1/+0 por cada Terreno Planicíe existente no campo do jogador.", "", Tipo_Criatura.Réptil);

            var x9 = new Criatura(ElementoCarta.Planície, 8, "Remiel", "Um anjo vindo do céu que ajuda os seres humanos a não sairem do seu caminho.", 7, 5, 
                "Luz Transcendente", "Voador", "Todas as criaturas que não sejam do tipo Planicíe recebem -2/-3 por cada turno em que esta criatura esteja em jogo.", "Não pode ser atingido por ataques térreos.", Tipo_Criatura.Divindade);

            var x10 = new Criatura(ElementoCarta.Pântano, 10, "Lucifer", "O anjo caído do céu e senhor do Inferno.", 5, 7, //10 de ataque e 10 de resistência na verdade
                "Meteoritos", "Puxar para o Inferno", "Todas as criaturas em jogo excepto esta recebem +0/-8 e não podem atuar no turno seguinte.", "A carta alvo vai imediatamente para o cemitério e não pode ser reavida de novo.", Tipo_Criatura.Demónio);

            var x11 = new Criatura(ElementoCarta.Pântano, 6, "Zombie PopStar", "Um morto-vio capaz de dançar e cantar.", 5, 7, 
                "Revoguar Atributos", "Atropelar", "Todas as auras e artefactos da criatura alvo são destruídos.", "Se os danos ultrapassarem  a resistência da carta alvo, o dano restante pode ser transferido para outra criatura.", Tipo_Criatura.Morto_Vivo);

            var x12 = new Criatura(ElementoCarta.Pântano, 1, "Alma Perdida", "Uma alma que percorre os corredores de ruínas em busca de sossêgo.", 1, 0, 
                "Possuír", "", "A criatura alvo tem 30% chance de ser controlada.", "", Tipo_Criatura.Supernatural);

            var x13 = new Criatura(ElementoCarta.Água, 3, "Tubarão Malhado", "Quando uma vaca e um tubarão martelo fazem cross-over, o resultado é algo violento e diabólico.", 5, 0, 
                "Iniciativa", "Mordida Feroz", "Esta Criatura atua primeiro que as outras.", "A criatura alvo recebe +0/-2 e o artefacto tem 35% chance de ser perdido.", Tipo_Criatura.Aquático);

            var x14 = new Criatura(ElementoCarta.Água, 1, "Abutre Veloz", "Um necrófago que anda à espera de comer o corpo de vitímas que sucumbam ao calor.", 1, 2, 
                "Voador", "Iniciativa", "Não pode ser atingido por ataques térreos.", "Esta Criatura atua primeiro que as outras.", Tipo_Criatura.Voador);

            var x15 = new Criatura(ElementoCarta.Água, 1, "Febre Louca", "Uma pequena bactéria que afeta tudo e todos!", 0, 2, 
                "Enlouquecer", "", "A carta alvo é atacada com o seu próprio atributo de ataque em vez do ataque desta criatura.", "", Tipo_Criatura.Vírus);

            Criaturas.Add(x1);
            Criaturas.Add(x2);
            Criaturas.Add(x3);

            Criaturas.Add(x4);
            Criaturas.Add(x5);
            Criaturas.Add(x6);

            Criaturas.Add(x7);
            Criaturas.Add(x8);
            Criaturas.Add(x9);

            Criaturas.Add(x10);
            Criaturas.Add(x11);
            Criaturas.Add(x12);

            Criaturas.Add(x13);
            Criaturas.Add(x14);
            Criaturas.Add(x15);

            return Criaturas;
        }

    }
}
