﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    class Feitiço : Carta
    {

        private string Descricao;

        public Feitiço(ElementoCarta el, int custo, string nome, string desc, string des):base( el,custo,nome,desc)
        {

            Descricao = des;

        }

        public Feitiço()
        {
        }

        public override string ToString()
        {

            return $"» Feitiço:\n\n   Nome: {Nome}\n   Cor: {Elemento}\n   Custo: {Mana}\n\n   {Descricao}\n   \n\n   Descrição: {DescricaoCarta}\n\n \n";

        }

        public List<Feitiço> Criar_Feitiços()
        {

            List<Feitiço> Feitiços = new List<Feitiço>();

            var x1 = new Feitiço(ElementoCarta.Floresta, 2, "Chuva de Pedragulhos", "Uma abundância de pedras caiem em cima de pessoas que passaram a carne picada...","Uma pedra gigante cai em cima das criaturas do seu oponente fazendo +0/-1");
            var x2 = new Feitiço(ElementoCarta.Fogo, 3, "Cinzas Fedorentas", "Cinzas raras que emanam cheiro a alface podre.", "A criatura com resistência mais alta fica com metade da resistência.");
            var x3 = new Feitiço(ElementoCarta.Planície, 1, "Saliva Restauradora", "A saliva de um gato rançoso e velho...", "A criatura escolhida irá recuperar +3/+0");
            var x4 = new Feitiço(ElementoCarta.Pântano, 1, "Nevoeiro Sinistro", "Uma neblina profunda aparece, cortando a visão de todos.", "Todas os artefactos em campo desaparecem, não podendo ser utilizados de novo!");
            var x5 = new Feitiço(ElementoCarta.Água, 2, "Espuma de banho", "Espuma divertida para toda a familía!", "Diverte as criaturas inimigas fazendo +0/-2");

            Feitiços.Add(x1);
            Feitiços.Add(x2);
            Feitiços.Add(x3);
            Feitiços.Add(x4);
            Feitiços.Add(x5);


            return Feitiços;
        }

    }
}
