﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    class MagicaInstantanea : Carta
    {

        private string efeito;
        private string Descricao;

        public MagicaInstantanea(ElementoCarta el, int custo, string nome, string desc, string habilidade, string des):base( el,custo,nome,desc)
        {

            efeito = habilidade;
            Descricao = des;

        }

        public MagicaInstantanea()
        {
        }

        public override string ToString()
        {

            return $"» Mágica Instantânea:\n\n   Nome: {Nome}\n   Cor: {Elemento}\n   Custo: {Mana}\n\n   {efeito}: {Descricao}\n   \n\n   Descrição: {DescricaoCarta}\n\n \n";

        }

        public List<MagicaInstantanea> Criar_MagicaInstantanea()
        {

            List<MagicaInstantanea> MagIns = new List<MagicaInstantanea>();

            var x1 = new MagicaInstantanea(ElementoCarta.Fogo, 1, "Bola de Fogo Rápida", "Uma bola de fogo que aparece do nada!", "Cuspir", "A criatura atacante do oponente sofre +0/-2 e a carta defensora não sofre danos.");
            var x2 = new MagicaInstantanea(ElementoCarta.Floresta, 1, "Toxinas", "Coisas roxas aparecem do nada que são por acaso, venenosas!", "Expelir","Quando uma criatura do oponente for invocada, esta sofre -1/-1.");
            var x3 = new MagicaInstantanea(ElementoCarta.Planície, 2, "Interromper Luz", "Uma sensação de empatia é surgida no oponente.", "Cegar", "A criatura do oponente têm 50% chance de acertar na criatura defensora.");
            var x4 = new MagicaInstantanea(ElementoCarta.Pântano, 2, "Buraco Negro", "Um vortéx abre e suga tudo no seu caminho!", "Sugar", "Todas as cartas em jogo desaparecem sem puderem ser reavidas!");
            var x5 = new MagicaInstantanea(ElementoCarta.Água, 1, "Lágrimas de Crocodilo", "O desespero para mentir é forte.", "Chorar", "Todas as criaturas do oponente recebem +0/-1 permanentemente.");

            MagIns.Add(x1);
            MagIns.Add(x2);
            MagIns.Add(x3);
            MagIns.Add(x4);
            MagIns.Add(x5);


            return MagIns;
        }


    }
}
