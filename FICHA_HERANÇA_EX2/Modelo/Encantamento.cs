﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    class Encantamento : Carta
    {

        private string Habilidade1;
        private string Descricao1;
        private string Habilidade2;
        private string Descricao2;

        public Encantamento(ElementoCarta el, int custo, string nome, string desc, string habilidade1, string habilidade2, string des1, string des2) :base( el,custo,nome,desc)
        {

            Habilidade1 = habilidade1;
            Habilidade2 = habilidade2;
            Descricao1 = des1;
            Descricao2 = des2;

        }

        public Encantamento()
        {
        }

        public override string ToString()
        {

            return $"» Encantamento:\n\n   Nome: {Nome}\n   Cor: {Elemento}\n   Custo: {Mana}\n\n   {Habilidade1}: {Descricao1}\n   {Habilidade2}: {Descricao2}\n\n   Descrição: {DescricaoCarta}\n\n \n";

        }

        public List<Encantamento> Criar_Encantamentos()
        {

            List<Encantamento> Encantamentos = new List<Encantamento>();

            var x1 = new Encantamento(ElementoCarta.Floresta, 2, "Proteção do Urso","O poder dos ursos protege-te!","Proteção","Retorno", "A criatura ganha +0/+1 por cada ataque resistido.","A criatura ataca a criatura atacante em retorno.");
            var x2 = new Encantamento(ElementoCarta.Fogo, 1, "Círculo de Chamas", "Um círculo de chamas em teu redor!", "Queimar", "", "A criatura atacante receberá - 1 / +0.", "");
            var x3 = new Encantamento(ElementoCarta.Planície, 2, "Sussuro Vitalício", "Uma magia antiga que garante vitalidade ao seu utilizador.", "Vitalidade", "Aura", "A criatura recebe -2/+5",  "A criatura não pode ser atacada por criaturas do tipo Voador.");
            var x4 = new Encantamento(ElementoCarta.Pântano, 1, "Grito de Dor", "Ensurdece todos os teus alvos!", "Ensurdecer", "", "A criatura atacante tem 75% chance de atingir a criatura defensiva e recebe -1/+0.",  "");
            var x5 = new Encantamento(ElementoCarta.Água,1, "Roubar", "Rouba tudo o que vires à tua frente!", "Roubar Tesouro", "", "A criatura tem 25% chance de poder roubar os artefactos da criatura que esta ataque.",  "");

            Encantamentos.Add(x1);
            Encantamentos.Add(x2);
            Encantamentos.Add(x3);
            Encantamentos.Add(x4);
            Encantamentos.Add(x5);


            return Encantamentos;
        }

    }
}
