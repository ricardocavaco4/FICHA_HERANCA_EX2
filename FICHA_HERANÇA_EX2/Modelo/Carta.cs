﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{

    public enum ElementoCarta
    {
        Pântano,
        Planície,
        Fogo,
        Água,
        Floresta,
        Sem_Cor
    }

    public abstract class Carta
    {


        public ElementoCarta Elemento { get; set; }
        public int Mana { get; set; }
        public string Nome { get; set; }

        public string DescricaoCarta { get; set; }

        public Carta(ElementoCarta el, int custo, string nome, string desc)
        {

            Elemento = el;
            Mana = custo;
            Nome = nome;
            DescricaoCarta = desc;

        }
        public Carta()
        {

        }

    }
}
