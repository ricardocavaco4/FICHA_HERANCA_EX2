﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    class Terreno : Carta
    {

        public Terreno(ElementoCarta el, int custo, string nome, string desc):base( el,custo,nome,desc)
        {
        }

        public Terreno()
        {
        }

        public override string ToString()
        {

            return $"» Terreno:\n\n   Nome: {Nome}\n   Cor: {Elemento}\n   Custo: {Mana}\n\n    \n   \n\n   Descrição: {DescricaoCarta}\n\n \n";

        }

        public List<Terreno> Criar_Terrenos()
        {

            List<Terreno> Terrenos = new List<Terreno>();

            var x1 = new Terreno(ElementoCarta.Floresta,0, "Floresta", "Uma floresta normal e simples.");
            var x2 = new Terreno(ElementoCarta.Fogo, 0, "Vulcão", "Um vulcão em erupção.");
            var x3 = new Terreno(ElementoCarta.Planície, 0, "Planície", "Uma planície radiante e vasta.");
            var x4 = new Terreno(ElementoCarta.Pântano, 0, "Pântano", "Um cheiro horrendo permeia deste pântano.");
            var x5 = new Terreno(ElementoCarta.Água, 0, "Rio", "O rio Tejo.");
            Terrenos.Add(x1);
            Terrenos.Add(x2);
            Terrenos.Add(x3);
            Terrenos.Add(x4);
            Terrenos.Add(x5);


            return Terrenos;
        }

    }
}
