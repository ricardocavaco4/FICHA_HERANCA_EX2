﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FICHA_HERANÇA_EX2
{
    public enum Tipo_Criatura
    {

        Humanóide,
        Animal,
        Mecânico,
        Inseto,
        Voador,
        Supernatural,
        Planta,
        Aquático,
        Réptil,
        Demónio,
        Dragão,
        Divindade,
        Objeto,
        Vírus,
        Morto_Vivo

    }

    public class TipoCriatura
    {

        public Tipo_Criatura Tipo { get; set; }

        public TipoCriatura(Tipo_Criatura tipo)
        {

            Tipo = tipo;

        }

    }
}
